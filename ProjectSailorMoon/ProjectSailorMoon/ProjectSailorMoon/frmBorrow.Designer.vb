﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmBorrow
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
        Me.txtName = New System.Windows.Forms.TextBox()
        Me.lblName = New System.Windows.Forms.Label()
        Me.lblNum = New System.Windows.Forms.Label()
        Me.btnCommand = New System.Windows.Forms.Button()
        Me.txtNum = New System.Windows.Forms.TextBox()
        Me.btnClear = New System.Windows.Forms.Button()
        Me.cboPCode = New System.Windows.Forms.ComboBox()
        Me.txtQuan = New System.Windows.Forms.TextBox()
        Me.txtRoom = New System.Windows.Forms.TextBox()
        Me.lblPCode = New System.Windows.Forms.Label()
        Me.lblQuan = New System.Windows.Forms.Label()
        Me.lblRoom = New System.Windows.Forms.Label()
        Me.btnPlus = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'ToolStrip1
        '
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(396, 25)
        Me.ToolStrip1.TabIndex = 0
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'txtName
        '
        Me.txtName.Location = New System.Drawing.Point(125, 56)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(141, 20)
        Me.txtName.TabIndex = 1
        '
        'lblName
        '
        Me.lblName.AutoSize = True
        Me.lblName.Location = New System.Drawing.Point(23, 59)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(75, 13)
        Me.lblName.TabIndex = 2
        Me.lblName.Text = "Student Name"
        '
        'lblNum
        '
        Me.lblNum.AutoSize = True
        Me.lblNum.Location = New System.Drawing.Point(26, 87)
        Me.lblNum.Name = "lblNum"
        Me.lblNum.Size = New System.Drawing.Size(84, 13)
        Me.lblNum.TabIndex = 3
        Me.lblNum.Text = "Student Number"
        '
        'btnCommand
        '
        Me.btnCommand.Location = New System.Drawing.Point(76, 346)
        Me.btnCommand.Name = "btnCommand"
        Me.btnCommand.Size = New System.Drawing.Size(75, 23)
        Me.btnCommand.TabIndex = 4
        Me.btnCommand.Text = "Save"
        Me.btnCommand.UseVisualStyleBackColor = True
        '
        'txtNum
        '
        Me.txtNum.Location = New System.Drawing.Point(125, 87)
        Me.txtNum.Name = "txtNum"
        Me.txtNum.Size = New System.Drawing.Size(141, 20)
        Me.txtNum.TabIndex = 5
        '
        'btnClear
        '
        Me.btnClear.Location = New System.Drawing.Point(237, 346)
        Me.btnClear.Name = "btnClear"
        Me.btnClear.Size = New System.Drawing.Size(75, 23)
        Me.btnClear.TabIndex = 6
        Me.btnClear.Text = "Clear"
        Me.btnClear.UseVisualStyleBackColor = True
        '
        'cboPCode
        '
        Me.cboPCode.FormattingEnabled = True
        Me.cboPCode.Location = New System.Drawing.Point(52, 161)
        Me.cboPCode.Name = "cboPCode"
        Me.cboPCode.Size = New System.Drawing.Size(99, 21)
        Me.cboPCode.TabIndex = 7
        '
        'txtQuan
        '
        Me.txtQuan.Location = New System.Drawing.Point(195, 162)
        Me.txtQuan.Name = "txtQuan"
        Me.txtQuan.Size = New System.Drawing.Size(26, 20)
        Me.txtQuan.TabIndex = 8
        '
        'txtRoom
        '
        Me.txtRoom.Location = New System.Drawing.Point(288, 162)
        Me.txtRoom.Name = "txtRoom"
        Me.txtRoom.Size = New System.Drawing.Size(61, 20)
        Me.txtRoom.TabIndex = 9
        '
        'lblPCode
        '
        Me.lblPCode.AutoSize = True
        Me.lblPCode.Location = New System.Drawing.Point(49, 142)
        Me.lblPCode.Name = "lblPCode"
        Me.lblPCode.Size = New System.Drawing.Size(72, 13)
        Me.lblPCode.TabIndex = 10
        Me.lblPCode.Text = "Product Code"
        '
        'lblQuan
        '
        Me.lblQuan.AutoSize = True
        Me.lblQuan.Location = New System.Drawing.Point(192, 142)
        Me.lblQuan.Name = "lblQuan"
        Me.lblQuan.Size = New System.Drawing.Size(46, 13)
        Me.lblQuan.TabIndex = 11
        Me.lblQuan.Text = "Quantity"
        '
        'lblRoom
        '
        Me.lblRoom.AutoSize = True
        Me.lblRoom.Location = New System.Drawing.Point(288, 142)
        Me.lblRoom.Name = "lblRoom"
        Me.lblRoom.Size = New System.Drawing.Size(35, 13)
        Me.lblRoom.TabIndex = 12
        Me.lblRoom.Text = "Room"
        '
        'btnPlus
        '
        Me.btnPlus.Location = New System.Drawing.Point(26, 158)
        Me.btnPlus.Name = "btnPlus"
        Me.btnPlus.Size = New System.Drawing.Size(20, 23)
        Me.btnPlus.TabIndex = 13
        Me.btnPlus.Text = "+"
        Me.btnPlus.UseVisualStyleBackColor = True
        '
        'frmBorrow
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(396, 394)
        Me.Controls.Add(Me.btnPlus)
        Me.Controls.Add(Me.lblRoom)
        Me.Controls.Add(Me.lblQuan)
        Me.Controls.Add(Me.lblPCode)
        Me.Controls.Add(Me.txtRoom)
        Me.Controls.Add(Me.txtQuan)
        Me.Controls.Add(Me.cboPCode)
        Me.Controls.Add(Me.btnClear)
        Me.Controls.Add(Me.txtNum)
        Me.Controls.Add(Me.btnCommand)
        Me.Controls.Add(Me.lblNum)
        Me.Controls.Add(Me.lblName)
        Me.Controls.Add(Me.txtName)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Name = "frmBorrow"
        Me.Text = "Borrow"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents txtName As System.Windows.Forms.TextBox
    Friend WithEvents lblName As System.Windows.Forms.Label
    Friend WithEvents lblNum As System.Windows.Forms.Label
    Friend WithEvents btnCommand As System.Windows.Forms.Button
    Friend WithEvents txtNum As System.Windows.Forms.TextBox
    Friend WithEvents btnClear As System.Windows.Forms.Button
    Friend WithEvents cboPCode As System.Windows.Forms.ComboBox
    Friend WithEvents txtQuan As System.Windows.Forms.TextBox
    Friend WithEvents txtRoom As System.Windows.Forms.TextBox
    Friend WithEvents lblPCode As System.Windows.Forms.Label
    Friend WithEvents lblQuan As System.Windows.Forms.Label
    Friend WithEvents lblRoom As System.Windows.Forms.Label
    Friend WithEvents btnPlus As System.Windows.Forms.Button
End Class
