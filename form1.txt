' FORM 1
Imports System.Data.OleDb


Public Class Form1
    Dim dbPath As String = Application.StartupPath & "\Borrowers.accdb" 
    Dim connectionString As String = "Provider=Microsoft.ACE.OLEDB.12.0; DATA SOURCE = " & dbPath

    Private Function performQuery(ByVal connectionString As String, ByVal sqlCommand As String) As OleDb.OleDbDataReader
        Dim dbConnection As OleDbConnection
        Dim dbCommand As New OleDbCommand()
        Dim dbDataReader As OleDb.OleDbDataReader = Nothing

        Try
            dbConnection = New OleDbConnection(connectionString)
            dbCommand.CommandText = sqlCommand
            dbCommand.Connection = dbConnection
            dbConnection.Open()
            dbDataReader = dbCommand.ExecuteReader
            Return dbDataReader
            dbConnection.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try

        Return Nothing
    End Function

    Private Function performNonQuery(ByVal connectionString As String, ByVal sqlCommand As String) As Boolean
        Dim dbConnection As OleDbConnection
        Dim dbCommand As New OleDbCommand()

        Try
            dbConnection = New OleDbConnection(connectionString)
            dbCommand.CommandText = sqlCommand
            dbCommand.Connection = dbConnection
            dbCommand.Connection.Open()
            dbCommand.ExecuteNonQuery()
            dbConnection.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Return False
        End Try

        Return True
    End Function

    ' BOTTON CONFIRM
    Private Sub btnConfirm1_Click(sender As Object, e As EventArgs) Handles btnConfirm1.Click
        ' TEXT BOX FOR USER NAME   
        If txtUserName.Text.Trim.Length = 0 Then
            MessageBox.Show("Please type your UserName.", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End If
        ' TEXT BOX FOR PASSWORD
        If txtPassword.Text.Trim.Length = 0 Then
            MessageBox.Show("Please type your Password.", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End If
        'Dim dbDataReader As OleDb.OleDbDataReader = Nothing
        'Dim Fu As String
        'Dim sqlCommand As String = "SELECT * FROM Borrowers WHERE UserName = '" & txtUserName.Text & "'"
        'dbDataReader = performQuery(connectionString, sqlCommand)
        'If dbDataReader.HasRows = 0 Then
        'sqlCommand = "SELECT * FROM Borrowers WHERE Password = '" & txtPassword.Text & "'"
        'dbDataReader = performQuery(connectionString, sqlCommand)
        '   If dbDataReader.HasRows = 0 Then
        ' If combox1.SelectedIndex.ToString = "0" Then
        'Fu = "Family"
        'ElseIf combox1.SelectedIndex.ToString = "1" Then
        '   Fu = "Friends"
        'ElseIf combox1.SelectedIndex.ToString = "2" Then
        '   Fu = "School"
        'ElseIf combox1.SelectedIndex.ToString = "3" Then
        '   Fu = "Work"
        'ElseIf combox1.SelectedIndex.ToString = "4" Then
        '   Fu = "Org"
        'End If

        'sqlCommand = "INSERT INTO Borrowers (UserName, Password ) VALUES ('" & txtUserName.Text & "', '" & txtPassword.Text & "')"
        ' If performNonQuery(connectionString, sqlCommand) Then
        'MessageBox.Show("Proceed.", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)
        'txtUserName.Clear()
        ' txtPassword.Clear()
        'Exit Sub

        'Else
        '     MessageBox.Show("Unable to Proceed.", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
        '      txtUserName.Focus()
        '       Exit Sub
        '    End If
        ' Else
        ' MessageBox.Show("Name aleady existing.", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)
        '  txtPassword.Focus()
        '   Exit Sub
        'End If


        ' FORM 1 TO FORM 2
        Form2.Show()
        Me.Hide()
    End Sub
End Class
